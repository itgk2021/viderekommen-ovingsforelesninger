# Oppgave 2
#Lag en funksjon som tar inn en liste og adderer hvert tredje tall i listen. 
# Hint: traversering, sum()

testliste = [i for i in range(31)]

def tregangen(liste):
    print(liste[::3])
    return sum(liste[::3])

print(tregangen(testliste))