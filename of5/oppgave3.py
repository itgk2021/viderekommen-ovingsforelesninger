#I denne oppgaven skal vi lage en forenklet versjon av yatzy
#Først trenger vi en funksjon som triller 5 terninger for oss.*

#a) Lag en funksjon, `roll_dice`,  som lager en liste som inneholder
# 5 elementer der hvert element har en tilfeldig tall-verdi mellom 1 og 6.
#Hint: import random, random.randint(1, 6) 


import random

def roll_dice():
    dice = []
    for i in range(5):
        dice.append(random.randint(1,6))
    return dice

kast = roll_dice()
print(kast)



#Videre trenger vi en funksjon for å telle antall forekomster
#  av en gitt verdi, f.eks. enere og toere*
#b) Lag en funksjon som tar inn listen med tall og et heltall mellom 1 og 6,
#  og returnerer antallet terninger som har den heltallsverdien 

def tell_antall(liste, tall):
    antall = 0
    for terning in liste:
        if terning == tall:
            antall += 1
    return antall

print(tell_antall(kast, 4))

#c) Til slutt vil vi kunne spille hele den første delen av et Yatzy-spill

#Lag en funksjon som kaster terningene seks ganger, og gir poeng for antall 
# enere i det første kastet, toere i det andre, osv. 
#- Print poeng for hvert kast
#- Returner summen

def Yatzy():
    score = 0
    for i in range(1,7):
        kast = roll_dice()
        antall = tell_antall(kast, i)
        print(kast)
        print("Det var", antall, "terninger med verdi", i)
        score += antall * i
    return score

print(Yatzy())

#Vi starter 15.11