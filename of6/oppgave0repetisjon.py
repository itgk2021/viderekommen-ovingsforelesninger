# Du har gitt en liste, `handleliste` som sier gjenstandene du skal kjøpe, og en liste `priser`
# som gir prisene til alle elementene i handlelisten. Det vil si at prisen til `handleliste[i]`
# er gitt av `priser[i]`hvor `i` er indeksen (plassen) til elementet i listene.

handleliste = ["Melk", "Bananer", "Brus", "Rundstykker", "Godteri", "Bønner", "Nøtter"]
priser = [17, 5, 25, 15, 55, 10, 30]

# Lag en funksjon `print_handleliste` som tar inn to parametre, handlelisten og prislisten,
# og printer ut handleliste-elementet med korresponderende pris med `:` mellom og `'kr'` på slutten. 


#kode her