#Du har fått beskjed om å ta vare på alle prisene til elementene i handlelisten

#a) Lag en ny dictionary `shopping_list` som inneholder følgende elementer som key med korresponderende pris som value. 
# Print ut prisen til 'Brød' etter handlelisten er definert.
# 'Brød': 23
# 'Melk': 17
# 'Egg': 39
# 'Smør': 30




#b) Lag en funksjon `update_price` som tar inn parameterne `item` og `price` og 
# oppdaterer prisen til `item` til den nye prisen `price` i `shopping_list`



#c) Lag en funkjson, `print_shopping_list`, som tar inn en dictionary `dict` og itererer gjennom 
# alle elementene i `dict` og printer ut item med korresponderende pris med `:` i mellom og `kr` på slutten.
# Hint: Item er key i dict og prisen er value
# Hint: en value til korresponderende key kan hentes ut ved `dict[key]`


#d) Lag en funksjon `total_price_shopping`, som tar inn to dictionaries: 
# En dictionary, `prices`. Den kan se slik ut: `{"Brød": 23, "Melk": 17, "Kattemat": 3}`
# En dictionary `numer_of_items` som representerer en "handlekurv", og kan se slik ut:
#  {"Melk": 2, "Egg": 4}. Dette betyr at det ligger to melk og 4 egg i handlekurven.
#Returner total pris på handlekurven, ved hjelp av de to dictionariene. 
