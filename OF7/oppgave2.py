#a) Skriv en funksjon `produce_number_file` som tar inn som inputparametere: 
# filename: et filnavn 
# a: et tall
# b: et tall

#Programmet skal opprette en fil med navn `filnavn` og skrive 100 tilfeldige tall mellom a og b, med linjeskift mellom, til en fil med navnet filnavn. 
# Hint: file.write("\n")



#b) Skriv en funksjon `append_to_file` som tar inn som inputparametere
# filename: et filnavn til en eksisterende fil
# a: et tall
#`b: et tall

#Programmet skal appende/legge til 20 nye tilfeldige tall i en fil, ett tall på hver linje


#c) Skriv en funksjon, `from_file_to_list` som tar inn en fil med ett tall på hver linje, og putter alle tallene i
#  en liste. Listen med tall skal returneres. Funksjonen skal ta inn `filename` som er navnet på filen du skal lese fra. 
# Test så programmet på `numbers.txt` filen.

#Det du ønsker å gjøre:
# Åpne filen du skal lese fra
# For hver linje i filen:  for linje in file.readlines():
#    Preprosesser (Fjern linjeskift ect) linje.strip()
#    Legge det i en liste eller annen datastruktur



#d) Skriv en funksjon, `from_file_to_list2`, som tar inn en fil med fem tall på hver linje, separert med mellomrom.
#  Funksjonen skal returnere en liste med alle tallene i filen. Prøv ut funksjonen på `morenumbers.txt`.


#e) Lag en funksjon main som spør brukeren om å gi inn filnavnet på filen det skal leses fra. Dersom filen eksisterer skal programmet
#  printe ut første linje i filen, hvis ikke filen eksisterer skal programmet printe ut en passende feilmelding og spørre på nytt. 
# Dersom brukeren skriver inn "exit" skal programmet hoppe ut.

